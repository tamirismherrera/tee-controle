// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyB0VRCYWkDYK3IbC_kq3oFZ0ddewdsnn5U',
    authDomain: 'controle-tmh-9be4a.firebaseapp.com',
    projectId: 'controle-tmh-9be4a',
    storageBucket: 'controle-tmh-9be4a.appspot.com',
    messagingSenderId: '426766777642',
    appId: '1:426766777642:web:f6c11b864ff72cbc403253',
    measurementId: 'G-PHY6GDCCCG'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
