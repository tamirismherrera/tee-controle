import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  isLoggedIn: Observable<any>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast : ToastController //exibe a mensagem de erro.
  ) { 
    this.isLoggedIn = this.auth.authState;
  }

  login(user){
    this.auth.signInWithEmailAndPassword(user.email, user.password).
    then(() => this.nav.navigateForward('home')).
    catch (() => this.showError());
  }

  private async showError(){
    const crtl = await this.toast.create({ // await "espera" tudo ocorrer pra depois executar o outro metodo.
      message: "Dados incorretos",
      duration: 3000
    });

    crtl.present(); //metodo para fazer a apresentação
  }

  createUser(user){
    this.auth.createUserWithEmailAndPassword(user.email, user.password).
    then(credentials => console.log(credentials));
  }
  recoverPass(data){
    this.auth.sendPasswordResetEmail(data.email).
    then(() => this.nav.navigateBack('auth')).
    catch(err => {
      console.log(err);
    });
  }

  logout(){
    this.auth.signOut()
    .then(()=> this.nav.navigateBack('auth'));
  }

}
